import Foundation

class Library: Codable {
    
    var name: String
    var info: String
    var like: Bool
    
    init(name: String,info: String, like: Bool) {
        self.name = name
        self.info = info
        self.like = like
    }
    
    enum CodingKeys: String, CodingKey {
        case name, info, like
    }
    
    required public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.name = try container.decode(String.self, forKey: .name)
        self.info = try container.decode(String.self, forKey: .info)
        self.like = try container.decode(Bool.self, forKey: .like)
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(self.name, forKey: .name)
        try container.encode(self.info, forKey: .info)
        try container.encode(self.like, forKey: .like)
    }
}






