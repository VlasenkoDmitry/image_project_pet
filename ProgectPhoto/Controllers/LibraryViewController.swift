import UIKit

class LibraryViewController: UIViewController {
    @IBOutlet weak var constrainViewChoice: NSLayoutConstraint!
    @IBOutlet weak var bottomConstrainScrollView: NSLayoutConstraint!
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var imageViewFirst: UIImageView!
    @IBOutlet weak var leftButton: UIButton!
    @IBOutlet weak var rightButton: UIButton!
    @IBOutlet weak var buttonLike: UIButton!
    
    var arrayImages = [String]()
    var arrayText = [String]()
    var counter = Int()
    var login = ""
    var index = Int()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerForKeyboardNotifications()
        counter = 0
        leftButton.isHidden = false
        rightButton.isHidden = false
        
        guard let login = UserDefaults.standard.value(forKey: "Login") as? String else { return }
        
        if UserDefaults.standard.value(forKey: login) == nil {
            imageViewFirst.image = UIImage(named: "noPhoto")
            leftButton.isHidden = true
            rightButton.isHidden = true
        } else {
            guard let array = UserDefaults.standard.decode(for: [Library].self, using: login) else { return }
            if index == nil {index = 0}
            imageViewFirst.image = LoadImages.loadImage(fileName: array[index].name)
            textField.text = array[index].info
            if array[index].like == false{
                buttonLike.isSelected = false
            }
            else {
                buttonLike.isSelected = true
            }
            counter = index
        }
    }
    
    @IBAction func leftButtonPressed(_ sender: UIButton) {
        var imageViewSecond = UIImageView()
        guard let login = UserDefaults.standard.value(forKey: "Login") as? String else { return }
        guard let array = UserDefaults.standard.decode(for: [Library].self, using: login) else { return }
        counter -= 1
        if counter < 0 {counter = array.count - 1}
        imageViewSecond.frame = imageViewFirst.frame
        imageViewSecond.image = imageViewFirst.image
        imageViewFirst.image = LoadImages.loadImage(fileName: array[counter].name)
        view.addSubview(imageViewSecond)
        UIView.animate(withDuration: 0.3) {
            imageViewSecond.frame.origin.x = -500
        } completion: { _ in
            imageViewSecond.removeFromSuperview()
        }
        textField.text = array[counter].info
        
        if array[counter].like == false {
            buttonLike.isSelected = false
        } else {
            buttonLike.isSelected = true
        }
        UserDefaults.standard.encode(for: array, using: login)
    }

    @IBAction func rightButtonPressed(_ sender: UIButton) {
        let imageViewSecond = UIImageView()
        guard let login = UserDefaults.standard.value(forKey: "Login") as? String else { return }
        guard let array = UserDefaults.standard.decode(for: [Library].self, using: login) else { return }
        counter += 1
        if counter > array.count - 1{counter = 0}
        imageViewSecond.frame = imageViewFirst.frame
        imageViewSecond.frame.origin.x = view.frame.size.width
        imageViewSecond.image = LoadImages.loadImage(fileName: array[counter].name)
        view.addSubview(imageViewSecond)
        UIView.animate(withDuration: 0.3) {
            imageViewSecond.frame.origin.x = self.imageViewFirst.frame.origin.x
        } completion: { _ in
            self.imageViewFirst.image = imageViewSecond.image
            imageViewSecond.removeFromSuperview()
        }
        textField.text = array[counter].info
        
        if array[counter].like == false {
            buttonLike.isSelected = false
        } else {
            buttonLike.isSelected = true
        }
        UserDefaults.standard.encode(for: array, using: login)
    }
    
    @IBAction func heartPressed(_ sender: UIButton) {
        guard let login = UserDefaults.standard.value(forKey: "Login") as? String else { return }
        guard let array = UserDefaults.standard.decode(for: [Library].self, using: login) else { return }
        array[counter].like = !array[counter].like
        if array[counter].like == false{
            buttonLike.isSelected = false
        } else {
            buttonLike.isSelected = true
        }
        UserDefaults.standard.encode(for:array, using: login)
    }
    
    private func registerForKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc private func keyboardWillShow(_ notification: NSNotification) {
        guard let userInfo = notification.userInfo,
              let animationDuration = (userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue,
              let keyboardScreenEndFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else { return }
        
        if notification.name == UIResponder.keyboardWillHideNotification {
            bottomConstrainScrollView.constant = 0
        } else {
            bottomConstrainScrollView.constant = keyboardScreenEndFrame.height - constrainViewChoice.constant
        }
        view.needsUpdateConstraints()
        UIView.animate(withDuration: animationDuration) {
            self.view.layoutIfNeeded()
        }
    }
}

extension LibraryViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        hideKeyBoard()
        return true
    }
    
    func hideKeyBoard() {
        guard let login = UserDefaults.standard.value(forKey: "Login") as? String else { return }
        guard let array = UserDefaults.standard.decode(for: [Library].self, using: login) else { return }
        guard let text = textField.text else { return }
        array[counter].info = text
        UserDefaults.standard.encode(for: array, using: login)
        textField.resignFirstResponder()
    }
}
