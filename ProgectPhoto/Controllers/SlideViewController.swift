import UIKit

class SlideViewController: UIViewController {
    @IBOutlet weak var imageCollectionNew: UICollectionView!
    var arrayImage = [UIImage]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
        
    @IBAction func plusPhotoButtonPressed(_ sender: UIButton) {
        guard let plus = self.storyboard?.instantiateViewController(withIdentifier: "ImageAddViewController") as? ImageAddViewController else { return }
        plus.delegate = self
        self.navigationController?.pushViewController(plus, animated: true)
    }
    
    @IBAction func libraryButtonPressed(_ sender: UIButton) {
        guard let library = self.storyboard?.instantiateViewController(withIdentifier: "LibraryViewController") as? LibraryViewController else { return }
        self.navigationController?.pushViewController(library, animated: true)
    }
    
}

extension SlideViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let login = UserDefaults.standard.value(forKey: "Login") as? String else { return 0 }
        guard let array = UserDefaults.standard.decode(for: [Library].self, using: login) else { return 0 }
        return array.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let login = UserDefaults.standard.value(forKey: "Login") as? String else { return UICollectionViewCell() }
        guard let array = UserDefaults.standard.decode(for: [Library].self, using: login) else { return UICollectionViewCell() }
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "NewCollectionViewCell", for: indexPath) as? NewCollectionViewCell else { return UICollectionViewCell() }
        guard let library = self.storyboard?.instantiateViewController(withIdentifier: "LibraryViewController") as? LibraryViewController else { return UICollectionViewCell() }
        guard let fileName = LoadImages.loadImage(fileName: array[indexPath.item].name) else { return UICollectionViewCell() }
        cell.configure(image: fileName, library: library, index: indexPath)
        cell.delegateTwo = self
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = (collectionView.frame.size.width - 15 ) / 2
        return CGSize(width: size, height: size)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 15
    }
}

extension SlideViewController: ImageAddViewControllerDelegate {
    func updateCollection() {
        imageCollectionNew.reloadData()
    }
}

extension SlideViewController: NewCollectionViewCellDelegate {
    func pushViewAfterTap(index: Int) {
        guard let library = self.storyboard?.instantiateViewController(withIdentifier: "LibraryViewController") as? LibraryViewController else { return }
        library.index = index
        self.navigationController?.pushViewController(library, animated: true)
    }
}
