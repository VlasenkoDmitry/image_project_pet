import UIKit

protocol ImageAddViewControllerDelegate {
    func updateCollection()
}

class ImageAddViewController: UIViewController {
    var chooseImage = UIImage()
    var arrayLibrary = [Library]()
    weak var delegate : SlideViewController?
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var infoImageTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let picker = UIImagePickerController()
        picker.sourceType =  .photoLibrary
        picker.allowsEditing = true
        picker.delegate = self
        present(picker, animated: true, completion: nil)
    }
    
    @IBAction func buttonAddPressed(_ sender: UIButton) {
        guard let login = UserDefaults.standard.value(forKey: "Login") as? String else { return }
        guard let image = LoadImages.saveImage(image: chooseImage ) else { return }
        guard let info = infoImageTextField.text else { return }
        let newElement = Library(name: image, info: info, like: false)
        Manager.shared.record(element: newElement,login: login)
        delegate?.updateCollection()
        navigationController?.popViewController(animated: true)
    }
}

extension ImageAddViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
            chooseImage = image
        } else if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            chooseImage = image
        }
        imageView.image = chooseImage
        picker.dismiss(animated: true, completion: nil)
    }
}


