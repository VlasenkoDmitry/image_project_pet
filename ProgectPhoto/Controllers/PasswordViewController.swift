import UIKit

class PasswordViewController: UIViewController {
    
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var againPasswordTextField: UITextField!
    @IBOutlet weak var invalidePOU: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func okButtonPressed(_ sender: UIButton) {
        var resultOfLoad = Bool()
        if nameTextField.text != nil , passwordTextField.text != nil, againPasswordTextField.text != nil {
            if passwordTextField.text == againPasswordTextField.text {
                if let login = nameTextField.text ,let password = passwordTextField.text {
                    resultOfLoad = LoadData.loadData(login: login, password: password)
                }
            } else {
                error(text: "different passwords")
            }
        } else {
            error(text: "invalid password or username")
        }
        
        if resultOfLoad == false{
            error(text: "nead other login")
        } else {
            invalidePOU.isHidden = true
            cleantextfields()
            navigationController?.popToRootViewController(animated: true)
        }
    }
    
    private func cleantextfields() {
        nameTextField.text = ""
        passwordTextField.text = ""
        againPasswordTextField.text = ""
    }
    
    func error (text: String) {
        invalidePOU.text = text
        invalidePOU.isHidden = false
    }
}

