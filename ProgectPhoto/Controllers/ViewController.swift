import UIKit

class ViewController: UIViewController {
    
    var secretData = [String: String]()
    
    @IBOutlet weak var loginTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var labelInvalidPOU: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        guard let text = UserDefaults.standard.value(forKey: "Data") else { return }
        print(text)
    }
    
    @IBAction func buttonLoginInPressed(_ sender: UIButton) {
        guard let libraryData = UserDefaults.standard.value(forKey: "Data") as? [String : String] else { return }
        for (login,password) in libraryData{
            if loginTextField.text == login, passwordTextField.text == password, loginTextField.text != nil, passwordTextField.text != nil {
                guard let slideView = self.storyboard?.instantiateViewController(withIdentifier: "SlideViewController") as? SlideViewController else {return}
                self.navigationController?.pushViewController(slideView, animated: true)
                UserDefaults.standard.set(login, forKey : "Login")
                cleanTextFields()
                break
            } else {
                labelInvalidPOU.isHidden = false
            }
        }
    }
    
    @IBAction func buttonRegistrationPressed(_ sender: UIButton) {
        cleanTextFields()
        guard let buttonRegistration = self.storyboard?.instantiateViewController(withIdentifier: "PasswordViewController") as? PasswordViewController else { return }
        self.navigationController?.pushViewController(buttonRegistration, animated: true)
    }
    
    private func cleanTextFields() {
        loginTextField.text = ""
        passwordTextField.text = ""
        labelInvalidPOU.isHidden = true
    }
}


