protocol NewCollectionViewCellDelegate: AnyObject {
    func pushViewAfterTap(index: Int)
}

import UIKit

class NewCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var newImage: UIImageView!
    @IBOutlet weak var contentVIew: UIView!
    
    var index = Int()
    weak var delegateTwo: NewCollectionViewCellDelegate?
    
    func configure (image: UIImage,library: LibraryViewController,index: IndexPath) {
        let recognizer = UITapGestureRecognizer(target: self, action: #selector(tapDetected))
        contentVIew.addGestureRecognizer(recognizer)
        newImage.image = image
        self.index = index.item
    }
    
    @objc func tapDetected() {
        delegateTwo?.pushViewAfterTap(index: index)
    }
    
}
