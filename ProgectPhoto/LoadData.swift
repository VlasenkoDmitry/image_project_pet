import Foundation

class LoadData {
    
    static func loadData(login: String, password: String) -> Bool {
        var mainDictionary : [String:String]
        if UserDefaults.standard.value(forKey: "Data") == nil {
            mainDictionary = [login : password]
            UserDefaults.standard.set(mainDictionary, forKey: "Data")
            return true
        } else {
            guard var mainDictionary = (UserDefaults.standard.value(forKey: "Data") as? [String : String]) else { return false }
            for (loginTrue,_) in mainDictionary {
                if login == loginTrue {
                    return false
                }
            }
            mainDictionary[login] = password
            UserDefaults.standard.set(mainDictionary, forKey: "Data")
            return true
        }
    }
}
