import Foundation

class Manager {
    
    static let shared = Manager()
    var array = [Library]()
    
    func record (element: Library, login: String) {
        if UserDefaults.standard.decode(for: [Library].self, using: login) != nil {
            guard var array = UserDefaults.standard.decode(for: [Library].self, using: login) else { return }
            array.append(element)
            UserDefaults.standard.encode(for: array, using: login)
        } else {
            array = [element]
            UserDefaults.standard.encode(for: array, using: login)
        }
    }
    
}

extension UserDefaults {
    func decode<T : Codable>(for type : T.Type, using key : String) -> T? {
        let defaults = UserDefaults.standard
        guard let data = defaults.object(forKey: key) as? Data else {return nil}
        let decodedObject = try? PropertyListDecoder().decode(type, from: data)
        return decodedObject
    }
    
    func encode<T : Codable>(for type : T, using key : String) {
        let defaults = UserDefaults.standard
        let encodedData = try? PropertyListEncoder().encode(type)
        defaults.set(encodedData, forKey: key)
        defaults.synchronize()
    }
    
}
